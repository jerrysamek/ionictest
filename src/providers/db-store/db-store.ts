import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';

/**
 * Database provider based on SQLite plugin.
 *
 * @author Tomas Samek (jerry.samek@gmail.com)
 */
@Injectable()
export class DBStoreProvider {

  private static DB_NAME: string = 'data.db';
  private static DB_LOCATION: string = 'default';

  private dbInstance: SQLiteObject = null;

  constructor(public sqLite: SQLite) {
    console.log('Hello DBStoreProvider Provider');
  }

  /**
   * This is main function to open connection to database and to prepare database to use. It also performs data
   * migration if it is necessary.
   *
   * This function should be called only on application start when splash screen is displayed.
   *
   * @return Promise<any> after connection is opened.
   */
  public init(): Promise<any> {
    return this.doInit()
      .then((db: SQLiteObject) => this.doMigration(db));
  }

  /**
   * This function returns promise with current instance.
   *
   * @return Promise<SQLiteObject> active database connection
   */
  public getConnection() {
    if (this.dbInstance != null) {
      return Promise.resolve(this.dbInstance);
    }
    return this.doInit();
  }

  private doInit(): Promise<SQLiteObject> {
    return this.sqLite
      .create({
        name: DBStoreProvider.DB_NAME,
        location: DBStoreProvider.DB_LOCATION
      })
      .then((db: SQLiteObject) => this.dbInstance = db);
  }

  private doMigration(db: SQLiteObject): Promise<any> {
    // TODO some hints: http://apandichi.github.io/database-migrations-in-cordova/
    return db.executeSql('create table if not exists my_notes(title VARCHAR(32), description VARCHAR(1024), created INTEGER, updated INTEGER, category VARCHAR(32))', [])
      .then(() => console.log('Table my_notes created!'));
  }
}

