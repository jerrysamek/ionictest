import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

/**
 * @author Tomas Samek (jerry.samek@gmail.com)
 */
@Component({
  selector: 'page-note-detail',
  templateUrl: 'note-detail.html',
})
export class NoteDetailPage {
  detail: { title: string, description: string, category: string };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.detail = navParams.get('item');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoteDetailPage');
  }

}
