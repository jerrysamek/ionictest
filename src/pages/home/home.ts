import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {DBStoreProvider} from "../../providers/db-store/db-store";
import {FormControl, FormGroup} from "@angular/forms";
import {NoteDetailPage} from "../note-detail/note-detail";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  lastUpdate: number = 0;
  noteForm: FormGroup = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
  });

  notes: Array<{ title: string, description: string, category: string, timestamp: number }>;

  constructor(public navCtrl: NavController, public dbProvider: DBStoreProvider) {
    this.notes = [];
  }
  
  public loadNotes() {
    this.dbProvider.getConnection()
      .then(connection => connection
        .executeSql("select * from my_notes where updated > ?", [this.lastUpdate]))
      .then(resultSet => {
        for (let i = 0; i < resultSet.rows.length; i++) {
          let row = resultSet.rows.item(i);
          this.notes.push({
            title: row.title,
            description: row.description,
            category: row.category,
            timestamp: Math.max(row.created, row.updated)
          });
          this.lastUpdate = Math.max(this.lastUpdate, row.updated);
        }
      })
      .catch(reason => console.error(reason));
  }

  public submitNode() {
    let newNote = this.noteForm.value;
    let timestamp = Date.now();

    this.dbProvider
      .getConnection()
      .then(connection => connection
        .executeSql("insert into my_notes values (?,?,?,?,?)",
          [newNote.title, newNote.description, timestamp, timestamp, "default"]))
      .then(() => {
        this.loadNotes();
      })
      .catch(reason => console.error(reason));
  }

  public showDetail(event, item)  {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(NoteDetailPage, {
      item: item
    });
  }
}
